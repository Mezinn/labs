#include <iostream>
#include <iomanip>
#include <list>
#include <string>
#include <climits>

using namespace std;

class Console {

public:
    static void clear() {
        system("clear");
    }

    static void log(string text, int setW = 0, int color = 0, int background = 0) {
        cout << "\033[" << color << ";" << background << "m";
        cout << setw(setW);
        cout << text;
        cout << "\033[0;m";
    }
};

class Matrix {

protected:
    int **array;
    int size;
public:

    Matrix(int size) {

        this->size = size;
        this->array = new int *[this->size];

        for (int i = 0; i < this->size; i++) {
            this->array[i] = new int[this->size];
        }

        for (int i = 0; i < this->size; ++i) {
            for (int j = 0; j < this->size; ++j) {
                this->array[i][j] = 0;
            }
        }
    }

    void read() {
        for (int i = 0; i < this->size; i++) {
            for (int j = 0; j < this->size; j++) {


                Console::clear();
                Console::log("Now you matrix is: \n\n");
                for (int x = 0; x < this->size; x++) {
                    for (int y = 0; y < this->size; y++) {
                        if (i == x && j == y) {
                            Console::log(to_string(this->array[x][y]), 5, 1, 31);
                            Console::log(" ");

                        } else {
                            Console::log(to_string(this->array[x][y]), 5);
                            Console::log(" ");
                        }
                    }
                    cout << endl;
                };

                while (!(cin >> this->array[i][j])) {
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                    Console::log("Invalid number. try again\n");
                }
            }
        }
    }

    void flush() {
        Console::clear();
        Console::log("Source matrix: \n\n");
        this->display();
        Console::log("\n");

        int sum = 0;
        for (int i = 0; i < this->size; i++) {
            for (int j = i + 1; j < this->size; j++) {
                sum += this->array[i][j];
            }
        }

        if (sum % 2 == 0) {
            for (int i = 0; i < this->size; i++) {
                this->array[i][i] = 0;
            }
            cout << "Sum is even, then we should flush matrix\n" << endl;
        } else {
            cout << "Sum is odd, then we should not change matrix\n" << endl;
        }

        this->display();
        Console::log("\n");
    }

    void display() {
        for (int i = 0; i < this->size; ++i) {
            for (int j = 0; j < this->size; ++j) {
                Console::log(to_string(this->array[i][j]), 5);
            }
            Console::log("\n");
        }
    }
};

int main() {
    int size = 0;

    Console::log("Enter size of matrix: ");
    while (!(cin >> size && size > 1)) {
        Console::clear();
        cin.clear();
        cin.ignore(INT_MAX, '\n');
        Console::log("Invalid matrix size. try again\n");
    }
    Matrix *matrix = new Matrix(size);
    Console::clear();
    matrix->read();
    matrix->flush();

    return 0;
}